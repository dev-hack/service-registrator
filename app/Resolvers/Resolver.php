<?php
declare(strict_types=1);

namespace App\Resolvers;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

interface Resolver
{
    function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): mixed;
}
